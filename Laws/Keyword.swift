//
//  Keyword.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Keyword: Codable{
    var Name: String
    var Code: String?
    var Keywords: [Keyword]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.Keywords != nil{
            for classificator in self.Keywords!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class KeywordRepository: Repository<Keyword>{
    init(){
        super.init(endpoint: "\(Host)/OpenData/GetClassificators.json?&Type=Keywords")
    }
}
