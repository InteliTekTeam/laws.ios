//
//  DocumentList.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct DocumentList: Codable{
    var Id: String
    var TotalCount: Int
    var PageSize: Int
    var PageNumber: Int
    var Documents: [Document]?
}

class DocumentListRepository:Repository<DocumentList>{
    var url = "\(Host)/OpenData/GetDocumentListByQuery.json?&Editions.Data=none&PageSize=15"
    init() {
        super.init(endpoint: self.url)
    }
    init(parameters: String, classs: [Int], page: Int, sortDesc: Bool){
        let classsString = classs.map({String($0)}).joined(separator: ",");
        let sort = "OrderBy=DateAdopted&&SortIsDesc=\(sortDesc)"
        super.init(endpoint: "\(self.url)\(parameters)&Classes=\(classsString)&PageNumber=\(page)&\(sort)")
    }
}
