//
//  HistoryService.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

class HistoryService {
    private var histories: [Document]
    private let limit: Int = 20
    init() {
        histories = [Document]()
        getDocuments { (documents) in
        }
    }
    
    func getDocuments(completionHandler: @escaping([Document])->Void){
        if histories.count > 0 {
            completionHandler(histories)
            return
        }
        getHistoriesFromFile { (documents, _) in
            if let documents = documents {
                for document in documents{
                    self.histories.append(document)
                }
            }
            completionHandler(self.histories)
        }
    }
    
    func append(document: Document){
        histories.insert(document, at: 0)
        while (histories.count>limit){
            histories.removeLast()
        }
        saveHistory()
    }
    
    func remove(_ index: Int){
        histories.remove(at: index)
        saveHistory()
    }
    
    private func saveHistory(){
        do {
            let codes = histories
            let encoder = JSONEncoder()
            let json = try encoder.encode(codes)
            saveToFile("history.txt", data: json)
        }
        catch {
            print(error)
        }
    }
    
    private func getDocumentPath(fileName: String) -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0].appendingPathComponent(fileName)
        return documentsDirectory
    }
    
    private func saveToFile(_ fileName: String, data: Data){
        do{
            if let jsonString = String(data: data, encoding: .utf8){
                let fileURL = self.getDocumentPath(fileName: fileName)
                try jsonString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            }
        }
        catch{
            print(error)
        }
    }
    
    private func getHistoriesFromFile(completionHandler: @escaping([Document]?, Error?)->Void){
        let decoder = JSONDecoder()
        do{
            let string = try String.init(contentsOf: self.getDocumentPath(fileName: "history.txt"))
            let data = string.data(using: .utf8)!
            
            if let documents = try decoder.decode([Document]?.self, from: data) as [Document]? {
                completionHandler(documents, nil)
            }
        }
        catch{
            print("Error: while trying to convert JSON to data")
            print(error)
            completionHandler(nil,nil)
        }
        
    }
}
