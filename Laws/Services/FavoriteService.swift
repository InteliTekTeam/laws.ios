//
//  DocumentService.swift
//  Laws
//
//  Created by User on 1/23/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

class FavoriteService {
    private var favorites: [Document]
    init() {
        favorites = [Document]()
        getDocuments { (documents) in
        }
    }
    
    func getDocuments(completionHandler: @escaping([Document])->Void){
        if favorites.count > 0 {
            completionHandler(favorites)
            return
        }
        getFavoritesFromFile { (documents, _) in
            if let documents = documents {
                for document in documents{
                    self.favorites.append(document)
                }
            }
            completionHandler(self.favorites)
        }
    }
    
    func getDocument(_ code: Int) -> Document? {
        if let _ = favorites.first(where: {$0.Code == code}){
            if let document = getDocumentFromFile(code){
                return document
            }
        }
        return nil
    }
    
    func isFavorite(code: Int) -> Bool {
        if let _ = favorites.first(where: {$0.Code == code}) {
            return true
        }
        return false
    }
    
    func append(document: Document) -> Bool{
        if let _ = favorites.first(where: {$0.Code==document.Code}){
            return false
        }
        favorites.append(document)
        saveFavorites()
        saveDocument(document)
        return true
    }

    func remove(document: Document) -> Bool{
        if let i = favorites.index(where: {$0.Code == document.Code}){
            favorites.remove(at: i)
            saveFavorites()
            return true
        }
        return false
    }
    
    private func saveDocument(_ document: Document){
        do {
            let encoder = JSONEncoder()
            let json = try encoder.encode(document)
            saveToFile("\(document.Code).txt", data: json)
        }
        catch {
            print(error)
        }
    }
    
    private func saveFavorites(){
        do {
            let codes = favorites
            let encoder = JSONEncoder()
            let json = try encoder.encode(codes)
            saveToFile("favorites.txt", data: json)
        }
        catch {
            print(error)
        }
    }
    
    private func getDocumentPath(fileName: String) -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0].appendingPathComponent(fileName)
        return documentsDirectory
    }
    
    private func saveToFile(_ fileName: String, data: Data){
        do{
            if let jsonString = String(data: data, encoding: .utf8){
                let fileURL = self.getDocumentPath(fileName: fileName)
                try jsonString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            }
        }
        catch{
            print(error)
        }
    }
    
    private func getFavoritesFromFile(completionHandler: @escaping([Document]?, Error?)->Void){
        let decoder = JSONDecoder()
        do{
            let string = try String.init(contentsOf: self.getDocumentPath(fileName: "favorites.txt"))
            let data = string.data(using: .utf8)!
            
            if let documents = try decoder.decode([Document]?.self, from: data) as [Document]? {
                completionHandler(documents, nil)
            }
        }
        catch{
            print("Error: while trying to convert JSON to data")
            print(error)
            completionHandler(nil,nil)
        }

    }
    
    private func getDocumentFromFile(_ code: Int) -> Document?{
        let decoder = JSONDecoder()
        do{
            let string = try String.init(contentsOf: self.getDocumentPath(fileName: "\(code).txt"))
            let data = string.data(using: .utf8)!
            
            if let document = try decoder.decode(Document?.self, from: data) as Document? {
                return document
            }
        }
        catch{
            print("Error: while trying to convert JSON to data")
            print(error)
        }
        return nil
        
    }
}
