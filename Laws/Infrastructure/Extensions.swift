//
//  Extensions.swift
//  Laws
//
//  Created by User on 1/16/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//
import UIKit
import Foundation

extension Array where Element == Item {
    func toUrlParameter() -> String {
        var result = ""
        if let first = self.first{
            result = first.code
        }
        for i in self.filter({$0.code != result}){
            result += ",\(i.code)"
        }
        return result
    }
    
    func toString() -> String{
        var result = ""
        if self.count>1{
            return "(\(self.count))"
        }
        if let first = self.first{
            result = first.name
        }
        for i in self.filter({$0.name != result}){
            result += ", \(i.name)"
        }
        return result
        
    }
    
    func isLast(item: Item) -> Bool {
        return self.index(where: {$0.code==item.code}) == self.count-1
    }
}

extension Dictionary where Key == FilterEnum, Value == Any?{
    func toUrlParameters() -> String {
        var result = ""
        for pair in self{
            if let value = pair.value{
                switch pair.key{
                case .types, .authorities, .statuses, .keywords, .generalClassifiers, .sourcePublications:
                    let items = value as! [Item]
                    if items.count>0{
                        result += "&\(pair.key)="
                        for item in items{
                            let delimeter = items.isLast(item: item) ? "" : ","
                            result+="\(item.code)\(delimeter)"
                        }
                    }
                case .dateAdopted, .dateLimit, .dateInclusion, .datePublication, .dateRegistration, .dateOfEntry:
                    let dates = value as! [Date?]
                    if let date = dates[0]{
                        result += "&\(pair.key).From=\(date.toString())"
                    }
                    if let date = dates[1]{
                        result += "&\(pair.key).To=\(date.toString())"
                    }
                case .number, .name, .numberRegistration, .editionText, .numberPublication:
                    if let text = (value as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
                        if !text.isEmpty {
                            result += "&\(pair.key)=\(text)"
                        }
                    }
                }
            }
        }
        return result
    }
    
    func any() -> Bool {
        let filters = self.filter({$0.value != nil})
        if filters.count>0{
            return true
        }
        return false
    }
    
    func compare(filter: [FilterEnum: Any?]) -> Bool{
        if self.count != filter.count{
            return false
        }
        
        for pair in filter {
            if let value = pair.value{
                switch pair.key{
                case .types, .authorities, .statuses, .keywords, .generalClassifiers, .sourcePublications:
                    let items1 = value as! [Item]
                    guard let items2 = self.first(where: {$0.key==pair.key})?.value as? [Item] else {return false}
                    if items1.count != items2.count{
                        return false
                    }
                    for item in items1 {
                        guard let _ = items2.first(where: {$0.code == item.code}) else {return false}
                    }
                case .dateAdopted, .dateLimit, .dateInclusion, .datePublication, .dateRegistration, .dateOfEntry:
                    let dates1 = value as! [Date?]
                    guard let dates2 = self.first(where: {$0.key==pair.key})?.value as? [Date?] else {return false}
                    if dates1.count != dates2.count {return false}
                    
                    if let date1 = dates1[0]{
                        guard let date2 = dates2[0] else {return false}
                        if date1 != date2 {return false}
                    }
                    if let date1 = dates1[1]{
                        guard let date2 = dates2[1] else {return false}
                        if date1 != date2 {return false}
                    }

                case .number, .name, .numberRegistration, .editionText, .numberPublication:
                    if let text1 = value as? String {
                        guard let text2 = value as? String else {return false}
                        if text1 != text2 {return false}
                    }
                }
            }
        }
        return true
    }
}

extension Date{
    func toString(_ format: String? = "dd.MM.yyyy") -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension String{
    func localized() -> String {
        let lang = UnitOfWork.shared().settingsService.getCurrentLang()
        let path = Bundle.main.path(forResource: lang.rawValue, ofType: "lproj")
        let bundle = Bundle(path: path!)
        let string = bundle?.localizedString(forKey: self, value: "**\(self)**", table: "Localizable")
        return string ?? "////\(self)////"
            //NSLocalizedString(self, tableName: "Localizable", value: "**\(self)**", comment: "")
    }
    
    func toDate(_ format: String? = "yyyy-MM-dd") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
    
    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    
    mutating func htmlTextSizeUp2x() -> String{
        var l = 0
        for range in self.lowercased().ranges(of: "font-size:"){
            var k = self.index(range.upperBound, offsetBy: l)
            let s = k
            while self[k] != "p"{
                k = self.index(k, offsetBy: 1)
            }
            var numberRange = s..<k
            let numberString = String(self[numberRange])
            if let number = NumberFormatter().number(from: numberString){
                let newNumber = number.floatValue * 2.0
                let newNumberString = String(newNumber)
                let r = newNumberString.count - numberString.count
                l+=r
                self.insert(contentsOf: String.init(repeating: " ", count: r), at: numberRange.upperBound)
                numberRange = s..<self.index(numberRange.upperBound, offsetBy: r)
                
                self.replaceSubrange(numberRange, with: "\(newNumberString)")
            }
        }
        return self
    }
    
    mutating func htmlAddUnicodeMetaIfNeeded() -> String {
        if self.index(of: "charset=unicode") != nil {
            return self
        }
        self = self.replacingOccurrences(of: "<html>", with: "<html><head><meta content=\"text/html; charset=unicode\" http-equiv=\"Content-Type\"></head>")
        return self
    }
    
    func getFirstNumber() -> String{
        if Int(self) != nil{
            return self
        }
        var result = ""
        for char in self {
            if let number = Int(String(char)){
                result += String(number)
            } else {
                return result
            }
        }
        return result
    }
    
    func appendBase64Syntact() -> String{
        return "data:image/png;base64,\(self)"
    }
}

extension UIImage{
    func getColored(color: UIColor) -> UIImage{
        let myImageView = UIImageView()
        let templateImage =   self.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        myImageView.image = templateImage
        myImageView.tintColor = color
        return templateImage
    }
}
