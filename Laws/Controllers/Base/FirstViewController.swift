//
//  FirstViewController.swift
//  Laws
//
//  Created by User on 11/18/17.
//  Copyright © 2017 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

protocol FirstViewControllerDelegate{
    func dropFilter()
}

class FirstViewController: CustomViewController {
    
    private var documentListView: DocumentListView?
    private var classSection: Class!
    private var allClasses: [Class]!
    private var properties: [FilterEnum: Any?] = [:]
    private var documentList: DocumentList?
    private var sortDesc: Bool = true
    var delegate: FirstViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.documentListView = DocumentListView(frame: self.view.frame)
        self.view.addSubview(self.documentListView!)
        self.documentListView!.autoPinEdgesToSuperviewEdges()
        self.documentListView!.delegate = self
        self.documentListView!.swipeDelegate = self
        self.documentListView!.navigationController = self.navigationController
        
        self.documentListView!.setDefaults()
    }
    
    init(classSection: Class, classes: [Class]) {
        super.init()
        self.classSection = classSection
        self.allClasses = classes
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if documentListView != nil && documentListView!.deselectCell(){
            
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func filterApply(properties: [FilterEnum : Any?]) {
        self.properties = properties
        self.documentListView?.setDefaults()
    }
    
    func compareWithFilter(properties: [FilterEnum: Any?])-> Bool{
        return self.properties.compare(filter: properties)
    }
}

extension FirstViewController: DocumentListViewDelegate{
    func didSelect(document: Document, edition: String) {
        self.openDocument(document: document, edition: edition)
    }
    
    func dropFilter() {
        self.properties.removeAll()
        self.documentListView?.setDefaults()
        delegate?.dropFilter()
    }
    
    func sort() {
        self.sortDesc = !self.sortDesc
        self.documentListView?.setDefaults()
    }
    
    
    func getTableParameters() -> DocumentTableParameters {
        return DocumentTableParameters(
            emptyText: "Нет данных! Измените параметры поиска".localized(),
            totalText: "Всего найдено документов".localized() + ": \(self.documentList?.TotalCount ?? 0)",
            totalAmount: self.documentList?.TotalCount ?? 0,
            swipeAble: true,
            sortDesc: sortDesc,
            filter: properties.any(),
            isSortAble: true
        )
    }
    
    func getDocuments(page: Int, completionHandler: @escaping ([Document]?, String?) -> Void) {
        var classSectionIds = [classSection.Id];
        if classSection.Id == 0 {
            classSectionIds = allClasses.map({$0.Id})
        }
        DocumentListRepository(parameters: properties.toUrlParameters(), classs: classSectionIds, page: page, sortDesc: sortDesc).get { (data, error) in
            if let error = error {
                var textError = "Произошла ошибка".localized()
                if let be = error as? BackendError {
                    textError = be.getErrorText()
                }
                completionHandler(nil, textError)
            }else if let data = data{
                DispatchQueue.main.async{
                    self.documentList = data
                    if let documents = data.Documents{
                        completionHandler(documents, nil)
                    }
                }
            }
        }
    }
}

extension FirstViewController: SwipeActionDelegate{
    func didDelete(document: Document, index: Int) {
    }
    
    func didFavorite(document: Document) {
        self.didFavoriteAction(code: document.Code, completionHandler: {
            
        })
    }
}
