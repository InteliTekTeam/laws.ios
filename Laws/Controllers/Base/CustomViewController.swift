//
//  CustomViewController.swift
//  Laws
//
//  Created by User on 1/14/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import PopupDialog

class CustomViewController: UIViewController {

    lazy var spinner: SpinnerView = {
        var spinner = SpinnerView(frame: .zero)
        self.view.addSubview(spinner)
        spinner.autoCenterInSuperview()
        return spinner
    }()

    private var text: String?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func infoPopDialog(title: String, message: String, completionHandler: (()->Void)?){
        let popup = PopupDialog(title: title, message: message)
        let buttonOne = DefaultButton(title: "Закрыть".localized(), dismissOnTap: true) {
            if let completionHandler = completionHandler {
                completionHandler()
            }
        }
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    func didFavoriteAction(code: Int, completionHandler: (@escaping()->Void)) {
        spinner.animate()
        if let document = UnitOfWork.shared().favoriteService.getDocument(code) {
            if UnitOfWork.shared().favoriteService.remove(document: document) {
                self.infoPopDialog(title: "Избранные".localized(), message: "Документ успешно удален из избранных".localized(), completionHandler: nil)
            }
            completionHandler()
            spinner.stop()
        }else{
            DocumentRepository(code: "\(code)", edition: "last").get(completionHandler: { (data, error) in
                DispatchQueue.main.async {
                    if let _ = error {
                        self.infoPopDialog(title: "Документ".localized(), message: "Документ не найден!".localized(), completionHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    if let data = data {
                        if UnitOfWork.shared().favoriteService.append(document: data) {
                            self.infoPopDialog(title: "Избранные".localized(), message: "Документ успешно добавлен в избранные".localized(), completionHandler: nil)
                        }
                    }
                    completionHandler()
                    self.spinner.stop()
                }
            })
        }
    }
    
    @objc func didBookAction(){
        let vc = PopupTextViewController(nibName: nil, bundle: nil)
        vc.setPlaceHolder("Поиск по коду документа".localized())
        let popup = PopupDialog(viewController: vc, buttonAlignment: UILayoutConstraintAxis.horizontal, hideStatusBar: false) {
        }

        let buttonCancel = CancelButton(title: "Отмена".localized(), dismissOnTap: true){
        }
        
        let buttonOne = DefaultButton(title: "Перейти".localized(), dismissOnTap: false) {
            guard let text = vc.textField.text else {return}
            if text.isEmpty{
                popup.shake()
                return
            }
            guard let code = Int(text) else {
                popup.shake()
                return
            }
            self.openDocumentByCode(code: "\(code)", animated: false)
            popup.dismiss(animated: true, completion: nil)
        }
        buttonOne.setTitleColor(BrandColor, for: .normal)
        
        popup.addButtons([buttonCancel, buttonOne])

        self.present(popup, animated: true, completion: nil)
    }
    
    func openDocumentByCode(code: String, animated: Bool) {
        self.spinner.animate()
        DocumentRepository(code: code, edition: "last").get(completionHandler: { (document, error) in
            DispatchQueue.main.async {
                if let _ = error {
                    self.infoPopDialog(title: "Ошибка", message: "Произошла ошибка открытия документа. Попробуйте позже.", completionHandler: nil)
                }
                if let document = document {
                    self.openDocument(document: document, edition: "last")
                }
                self.spinner.stop()
            }
        })
    }
    
    func openDocument(document: Document, edition: String){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let pc = mainStoryboard.instantiateViewController(withIdentifier: "DocumentPagesViewController") as! DocumentPagesViewController
        pc.setProperties(document: document, edition: edition)
        self.navigationController?.pushViewController(pc, animated: true)
        
    }
    
    
    @objc func favoreButton(object: Any?){
        if let button = object as? UIBarButtonItem{
            self.didFavoriteAction(code: button.tag, completionHandler: {
                if UnitOfWork.shared().favoriteService.isFavorite(code: button.tag){
                    button.image = #imageLiteral(resourceName: "starfilled")
                }else{
                    button.image = #imageLiteral(resourceName: "star")
                }
            })
        }
    }
    
}
