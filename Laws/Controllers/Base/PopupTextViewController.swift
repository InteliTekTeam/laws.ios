//
//  PopupTextViewController.swift
//  Laws
//
//  Created by User on 1/30/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class PopupTextViewController: UIViewController, UITextFieldDelegate {

    lazy var textField: UITextField = {
        var textField = UITextField(frame: .zero)
        self.view.addSubview(textField)
        textField.autoSetDimension(.height, toSize: 44.0)
        textField.autoPinEdgesToSuperviewEdges()
        textField.textAlignment = NSTextAlignment.center
        return textField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func errorText(){
        textField.backgroundColor = UIColor.red
        
        UIView.animate(withDuration: 0.3, animations: {
            self.textField.backgroundColor = UIColor.white
        })
    }
    
    func setPlaceHolder(_ text: String){
        textField.placeholder = text
    }
}
