//
//  DocumentPagesViewController.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/22/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class DocumentPagesViewController: PagingViewController {

    var document: Document?
    var edition: String?
    
    lazy private var documentViewController: DocumentViewController = {
        let vc = DocumentViewController()
        let navBar = self.navigationController?.navigationBar
        let topPosition = navBar?.frame.maxY ?? 0.0
        vc.setProperties(document: self.document!, edition: self.edition!, topPosition: topPosition)
        return vc
    }()
    
    lazy private var documentEditionViewController: DocumentEditionsViewController = {
        let vc = DocumentEditionsViewController()
        vc.setProperties(document: self.document!, edition: self.edition!)
        return vc
    }()
    
    lazy private var documentPropertyViewController: DocumentPropertyViewController = {
        let vc = DocumentPropertyViewController()
        vc.setProperties(document: self.document!)
        return vc
    }()
    
    lazy private var documentReferenceViewController: DocumentReferencesViewController = {
        let vc = DocumentReferencesViewController()
        vc.setProperties(document: self.document!)
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.widthForMenuBar = 150.0
        
        let favoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "star"), style: .plain, target: self, action: #selector(favoreButton(object:)))
        favoriteButton.tag = document!.Code
        if UnitOfWork.shared().favoriteService.isFavorite(code: document!.Code) {
            favoriteButton.image = #imageLiteral(resourceName: "starfilled")
        }
        let bookButton = UIBarButtonItem(image: #imageLiteral(resourceName: "book"), style: .plain, target: self, action: #selector(didBookAction))
        self.barButtons = [favoriteButton, bookButton]

        self.dataSource = [
            (menuTitle: "Содержание".localized(),vc: documentViewController),
            (menuTitle: "Реквизиты".localized(),vc: documentPropertyViewController),
            (menuTitle: "Редакции".localized(), vc: documentEditionViewController),
            (menuTitle: "Ссылки".localized(),vc: documentReferenceViewController)]
        
    }
    
    func setProperties(document: Document, edition: String){
        self.document = document
        self.edition = edition
    }
    
    private func fillControllers(){
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
