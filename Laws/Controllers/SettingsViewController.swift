//
//  SettingsViewController.swift
//  Laws
//
//  Created by User on 1/29/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    private enum SettingsEnum: Int{
        case about, language
    }
    
    lazy var table: UITableView = {
        var table = UITableView(frame: .zero)
        self.view.addSubview(table)
        table.tableFooterView = UIView(frame: .zero)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.register(AboutCell.self, forCellReuseIdentifier: "aboutCell")
        table.autoPinEdgesToSuperviewEdges()
        
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Настройки".localized()

        table.delegate = self
        table.dataSource = self
        table.estimatedRowHeight = 85.0
        table.rowHeight = UITableViewAutomaticDimension

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let setEnum = SettingsEnum(rawValue: indexPath.row)!
        switch setEnum {
        case .about:
            let cell = AboutCell(style: .default, reuseIdentifier: "aboutCell")
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        case .language:
            let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
            cell.textLabel?.text = "Язык".localized()
            cell.detailTextLabel?.text = UnitOfWork.shared().settingsService.getCurrentLang().getText()
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let setEnum = SettingsEnum(rawValue: indexPath.row)!
        if setEnum == .language{
            openLanguages()
        }
        table.deselectRow(at: indexPath, animated: true)
    }
    
    private func openLanguages(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
        let items = [
            Item(code: Lang.ru.rawValue, name: Lang.ru.getText(), list: nil),
            Item(code: Lang.kg.rawValue, name: Lang.kg.getText(), list: nil)
            ]
        let selectedItem = items.first(where: {$0.code == UnitOfWork.shared().settingsService.getCurrentLang().rawValue})
        vc.setProperties(items: items, choosed: [selectedItem!], isMultipleChoise: false, tag: 0, isFirstLevel: true)
        vc.title = "Выбор языка".localized()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SettingsViewController: ListViewControllerDelegate{
    func disappear(choosed: [Item]?) {
        if let selected = choosed?.first{
            if let lang = Lang(rawValue: selected.code){
                UnitOfWork.shared().settingsService.setLang(lang: lang)
                UnitOfWork.resetCache()
                self.table.reloadData()
            }
        }else{
            UnitOfWork.shared().settingsService.setLang(lang: Lang.ru)
            UnitOfWork.resetCache()
            self.table.reloadData()
        }
    }
    
    
}


