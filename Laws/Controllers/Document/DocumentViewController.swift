//
//  DocumentViewController.swift
//  Laws
//
//  Created by User on 1/21/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import WebKit
import CircleMenu
import PopupDialog
import MessageUI

class DocumentViewController: CustomViewController {

    lazy private var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        return webView
    }()
    
    private var html = ""
    private var shouldSetupConstraints = true
    private var contentOffSet:CGFloat!
    private var topPosition: CGFloat!
    private var topViewHeight: CGFloat!
    private var document: Document!
    private var edition: String!
    var button: UIButton!
    lazy var menu: CircleMenu = {
        var menu = CircleMenu(
            frame: CGRect(x: 10.0, y: 10.0, width: 50.0, height: 50.0),
            normalIcon:"menu",
            selectedIcon:"close",
            buttonsCount: 3,
            duration: 0,
            distance: 120
        )
        menu.startAngle = 90.0
        menu.endAngle = 180.0
        menu.backgroundColor = BrandColor
        menu.layer.cornerRadius = menu.frame.size.width / 2.0
        
        return menu
    }()
    
    lazy var searchBar: SearchBarView = {
        var bar = SearchBarView(frame: CGRect.zero)
        return bar
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = webView
        webView.navigationDelegate = self
        
        contentOffSet = 0.0
        
        topViewHeight = topPosition
        
        menu.delegate = self
        view.addSubview(menu)
        
        searchBar.delegate = self
        view.addSubview(searchBar)

        setConstraints()
        
    }

    func setConstraints() {
        if shouldSetupConstraints{
            webView.scrollView.contentInset = UIEdgeInsets(top: topPosition, left: 0.0, bottom: 0.0, right: 0.0)
            searchBar.autoPinEdge(toSuperviewEdge: .top, withInset: 0.0)
            searchBar.autoPinEdge(toSuperviewEdge: .left, withInset: 0.0)
            searchBar.autoPinEdge(toSuperviewEdge: .right, withInset: 0.0)
            searchBar.hide()
            shouldSetupConstraints = false
        }
        super.updateViewConstraints()
    }
    
    func getDocumentPath() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0].appendingPathComponent("page.html")
        return documentsDirectory
    }
    
    func setProperties(document: Document, edition: String, topPosition: CGFloat){
        self.topPosition = topPosition
        self.edition = edition
        self.title = "№\(document.Code)"
        loadingDocument(document: document, completionHandler: { (error) in
            if let _ = error {
                return
            }
            self.prepareHtml()
        })
        UnitOfWork.shared().historyService.append(document: document)
    }
    
    fileprivate func loadingDocument(document: Document, completionHandler: @escaping(Error?)->Void){
        if let document = UnitOfWork.shared().favoriteService.getDocument(document.Code){
            self.document = document
            completionHandler(nil)
        } else {
            self.spinner.animate()
            DocumentRepository(code: "\(document.Code)", edition: self.edition).get(completionHandler: { (data, error) in
                DispatchQueue.main.async {
                    if let _ = error {
                        var textError = "Документ не найден".localized()
                        if let be = error as? BackendError{
                            textError = be.getErrorText()
                        }
                        self.infoPopDialog(title: "Ошибка".localized(), message: "\(textError)", completionHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        completionHandler(error)
                    } else if let data = data {
                        self.document = data
                        completionHandler(nil)
                    }
                    self.spinner.stop()
                }
            })
        }
    }
}

extension DocumentViewController: CircleMenuDelegate{
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        switch atIndex {
        case 0:
            button.setImage(#imageLiteral(resourceName: "search"), for: .normal)
            button.tintColor = UIColor.white
            button.backgroundColor = BrandColor
        case 1:
            button.setImage(#imageLiteral(resourceName: "share"), for: .normal)
            button.tintColor = UIColor.white
            button.backgroundColor = BrandColor
        case 2:
            button.setImage(#imageLiteral(resourceName: "warning"), for: .normal)
            button.tintColor = UIColor.white
            button.backgroundColor = BrandColor
        default:
            break
        }
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        switch atIndex{
        case 0:
            self.searchingText()
        case 1:
            let vc = UIActivityViewController(activityItems: [#imageLiteral(resourceName: "logo2"), self.document.TitleForShare], applicationActivities: nil)
            self.present(vc, animated: true)
        case 2:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([MailRecipient])
                mail.setSubject("Ошибка".localized())
                mail.setMessageBody("<p><b>Ошибка в документе:</b></p><p>\(document.TitleForShare)</p>", isHTML: true)
                self.present(mail, animated: true)
            } else {
                self.infoPopDialog(title: "Ошибка".localized(), message: "Невозможно отправить сообщение".localized(), completionHandler: nil)
            }
        default:
            break
        }
    }
}

extension DocumentViewController: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension DocumentViewController: WKUIDelegate{
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        switch (action){
        case #selector(copy(_:)):
            return super.canPerformAction(action, withSender: sender)
        default:
            return false
        }
    }
    
    override func copy(_ sender: Any?) {
        super.copy(sender)
    }

}

extension DocumentViewController: WKNavigationDelegate, SearchBarViewDelegate {

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated{
            if let url = navigationAction.request.url {
                let split = url.absoluteString.components(separatedBy: ":")
                if split[0] == "cdb"{
                    openDocument(code: split[1].getFirstNumber())
                    decisionHandler(.cancel)
                    return
                }
            }
        }
        decisionHandler(.allow)
    }
    
    private func openDocument(code: String) -> Void {
        self.openDocumentByCode(code: code, animated: true)
    }
    
    private func prepareHtml(){
        DispatchQueue.main.async{
            var edition: Edition!
            if let editionCode = Int(self.edition){
                edition = self.document.Editions?.first(where: {$0.Code == editionCode})
            }else{
                edition = self.document.Editions?.first
            }
            if let edition = edition {
                var html = edition.Data ?? ""
                if let images = edition.Images{
                    for image in images{
                        html = html.replacingOccurrences(of: image.Name, with: image.Data.appendBase64Syntact())
                    }
                }
                html = html.replacingOccurrences(of: "cdb:\(self.document.Code)#", with: "#")
                html = html.replacingOccurrences(of: "name=", with: "id=")
                html = html.htmlAddUnicodeMetaIfNeeded()
                self.html = html.htmlTextSizeUp2x()
                
                do {
                    let fileURL = self.getDocumentPath()
                    try self.html.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
                }
                catch {
                    print(error)
                }
                
                let fileURL = self.getDocumentPath()
                self.webView.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
            }
        }
    }

    func highlightAllOccurencesOfString(str: String, completionHandler: @escaping(Int)->Void) {
        removeAllHighlights()
        let path = Bundle.main.path(forResource: "UIWebViewSearch", ofType: "js")
        
        var jsCode = ""
        do{
            jsCode = try String(contentsOfFile: path!)
        }catch
        {
            completionHandler(0)
        }
        
        self.webView.evaluateJavaScript(jsCode, completionHandler:nil)
        
        let startSearch = "uiWebview_HighlightAllOccurencesOfString('\(str)')"
        self.webView.evaluateJavaScript(startSearch, completionHandler: nil)
        
        self.webView.evaluateJavaScript("uiWebview_SearchResultCount", completionHandler: { (result, error) in
            if let error=error{
                print(error)
            }
            print(result!)
            if let c = result as? Int {
                completionHandler(c)
            }
        })
    }
    
    func scrollTo(index:Int)  {
        let scrollToFunction = "uiWebview_ScrollTo('\(index)')"
        self.webView.evaluateJavaScript(scrollToFunction, completionHandler: nil)
    }
    
    func removeAllHighlights() {
        let removeAllHighlightsFunction = "uiWebview_RemoveAllHighlights()"
        self.webView.evaluateJavaScript(removeAllHighlightsFunction, completionHandler: nil)
    }
    
    func searchingText(){
        let vc = PopupTextViewController(nibName: nil, bundle: nil)
        vc.setPlaceHolder("Поиск по тексту".localized())
        let popup = PopupDialog(viewController: vc, buttonAlignment: UILayoutConstraintAxis.horizontal, hideStatusBar: false) {
        }
        
        let buttonCancel = CancelButton(title: "Отмена".localized(), dismissOnTap: true){
        }
        
        let buttonOne = DefaultButton(title: "Поиск".localized(), dismissOnTap: false) {
            guard let text = vc.textField.text else {return}
            if text.isEmpty{
                popup.shake()
                return
            }
            self.highlightAllOccurencesOfString(str: text, completionHandler: {(k) in
                if k>0 {
                    self.searchBar.show(total: k)
                    self.scrollTo(index: k-1)
                }else{
                    self.infoPopDialog(title: "Поиск".localized(), message: "Поиск не дал результатов".localized(), completionHandler: nil)
                }
            })
            popup.dismiss(animated: true, completion: nil)
        }
        buttonOne.setTitleColor(BrandColor, for: .normal)
        
        popup.addButtons([buttonCancel, buttonOne])
        
        self.present(popup, animated: true, completion: nil)
    }

    func goto(_ index: Int, total: Int) {
        let i = total - index
        self.scrollTo(index: i)
    }
    
    func close() {
        self.removeAllHighlights()
    }
    
}
