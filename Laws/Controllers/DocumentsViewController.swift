//
//  DocumentsViewController.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/22/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class DocumentsViewController: PagingViewController {

    private var properties: [FilterEnum: Any?] = [:]
        
    override func viewDidLoad() {
        self.title = "Документы"
        self.dataSource = []
        
        let filterButton = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(filterButtonTapped))
        let bookButton = UIBarButtonItem(image: #imageLiteral(resourceName: "book"), style: .plain, target: self, action: #selector(didBookAction))
        self.barButtons = [filterButton, bookButton]
        
        self.spinner.animate()
        UnitOfWork.shared().classRep.getAll(completionHandler: { (data, error) in
            DispatchQueue.main.async {
                if let error = error {
                    var textError = "Произошла ошибка".localized()
                    if let be = error as? BackendError{
                        textError = be.getErrorText()
                    }
                    self.infoPopDialog(title: "Ошибка".localized(), message: textError, completionHandler: nil)
                } else if let classes = data {
                    for clas in classes{
                        let vc = FirstViewController(classSection: clas, classes: classes)
                        vc.delegate = self
                        self.dataSource?.append((menuTitle: clas.NameLocal, vc: vc))
                    }
                }
                super.viewDidLoad()
                if let _ = self.dataSource {
                    self.reloadData()
                }
                self.spinner.stop()
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.spinner.isHidden && self.dataSource?.count == 0{
            self.spinner.animate()
            UnitOfWork.shared().classRep.getAll(completionHandler: { (data, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        var textError = "Произошла ошибка".localized()
                        if let be = error as? BackendError{
                            textError = be.getErrorText()
                        }
                        self.infoPopDialog(title: "Ошибка".localized(), message: textError, completionHandler: nil)
                    } else if let classes = data {
                        for clas in classes{
                            let vc = FirstViewController(classSection: clas, classes: classes)
                            vc.delegate = self
                            self.dataSource?.append((menuTitle: clas.NameLocal, vc: vc))
                        }
                    }
                    if let _ = self.dataSource {
                        self.reloadData()
                    }
                    self.spinner.stop()
                }
            })
        }
    }
    
    @objc func filterButtonTapped(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        vc.delegate = self
        vc.setProperties(properties: self.properties)
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewPageAt(index: Int) {
        super.viewPageAt(index: index)
        if let dataSource = self.dataSource{
            if let controller = dataSource[index].vc as? FirstViewController{
                if !(controller.compareWithFilter(properties: self.properties)){
                    controller.filterApply(properties: properties)
                }
            }
        }
    }
}

extension DocumentsViewController: FilterViewControllerDelegate{
    func filterApply(properties: [FilterEnum : Any?]) {
        if let dataSource = self.dataSource{
            self.properties = properties
            let index = currentIndex()
            let controller = dataSource[index].vc as? FirstViewController
            controller?.filterApply(properties: properties)
        }
    }
}

extension DocumentsViewController: FirstViewControllerDelegate{
    func dropFilter() {
        self.properties.removeAll()
    }
}
