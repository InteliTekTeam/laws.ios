//
//  SpinnerView.swift
//  Laws
//
//  Created by User on 1/19/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

import PureLayout

class SpinnerView: UIView {
    
    lazy private var image: UIImageView = {
        var image = UIImageView(frame: CGRect.zero)
        image.autoSetDimension(.height, toSize: 32.0)
        image.autoSetDimension(.width, toSize: 32.0)

        image.image = #imageLiteral(resourceName: "spin")
        return image
    }()
    private var inProgress: Bool = false
    let screensize = UIScreen.main.bounds
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func didMoveToSuperview(){
        self.addSubview(image)
        
        let edgesInset: CGFloat = 16.0
        image.autoPinEdge(toSuperviewEdge: .top, withInset: edgesInset)
        image.autoPinEdge(toSuperviewEdge: .bottom, withInset: edgesInset)
        image.autoAlignAxis(toSuperviewAxis: .vertical)
    }
    
    func stop(){
        self.isHidden = true
        self.image.isHidden = true
        self.inProgress=false
    }
    
    func animate(){
        self.isHidden = false
        self.image.isHidden = false
        self.inProgress=true
        UIView.animate(withDuration: 1) { () -> Void in
            self.image.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        
        UIView.animate(withDuration: 1, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.image.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)}, completion: {finished -> Void in
                if self.inProgress {
                    self.animate()
                }
            })
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
