//
//  SearchView.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import PureLayout
protocol SearchBarViewDelegate{
    func goto(_ index: Int, total: Int)
    func close()
}

class SearchBarView: UIView {
    lazy var closeButton: UIButton = {
        var button = UIButton(frame: CGRect.zero)
        button.autoSetDimension(.width, toSize: 50.0)
        button.setImage(#imageLiteral(resourceName: "close2"), for: .normal)
        return button
    }()
    lazy var downButton: UIButton = {
        var button = UIButton(frame: CGRect.zero)
        button.autoSetDimension(.width, toSize: 50.0)
        button.backgroundColor = BrandColor
        button.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        return button
    }()
    lazy var upButton: UIButton = {
        var button = UIButton(frame: CGRect.zero)
        button.autoSetDimension(.width, toSize: 50.0)
        button.backgroundColor = BrandColor
        button.setImage(#imageLiteral(resourceName: "up"), for: .normal)
        return button
    }()
    lazy var label: UILabel = {
        var label = UILabel(frame: CGRect.zero)
        label.textColor = UIColor.white
        return label
    }()
    private var total: Int = 0
    private var currentValue: Int = 0
    private var current: Int {
        get{
            return currentValue
        }
        set{
            if total==0{
                return
            }
            if newValue>total {
                currentValue = 1
            } else if newValue<1{
                currentValue = total
            } else {
                currentValue = newValue
            }
            label.text = "\(currentValue) : \(total)"
        }
    }
    private let height: CGFloat = 66.0
    private var heightConstraint: NSLayoutConstraint!
    var delegate: SearchBarViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        current = 0
        total = 0
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        upButton.addTarget(self, action: #selector(upButtonTapped), for: .touchUpInside)
        downButton.addTarget(self, action: #selector(downButtonTapped), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
    }
    
    override func didMoveToSuperview() {
        self.addSubview(closeButton)
        self.addSubview(downButton)
        self.addSubview(upButton)
        self.addSubview(label)
        
        heightConstraint = self.autoSetDimension(.height, toSize: height)
        closeButton.autoPinEdges(toSuperviewMarginsExcludingEdge: .right)
        downButton.autoPinEdges(toSuperviewMarginsExcludingEdge: .left)
        upButton.autoPinEdge(.right, to: .left, of: downButton, withOffset: -15.0)
        upButton.autoPinEdge(toSuperviewMargin: .top)
        upButton.autoPinEdge(toSuperviewMargin: .bottom)
        
        label.autoAlignAxis(toSuperviewMarginAxis: .horizontal)
        label.autoAlignAxis(toSuperviewMarginAxis: .vertical)
    }

    @objc func closeButtonTapped(){
        hide()
        delegate?.close()
    }
    
    @objc func upButtonTapped(){
        current-=1
        delegate?.goto(current, total: total)
    }

    @objc func downButtonTapped(){
        current+=1
        delegate?.goto(current, total: total)
    }
    
    func show(total: Int){
        if total==0{
            return
        }
        self.total = total
        self.current = 1
        heightConstraint.constant += height
    }
    
    func hide(){
        self.label.text = ""
        self.total = 0
        self.currentValue = 0
        heightConstraint.constant -= height
    }
    
    required init?(coder Decoder: NSCoder) {
        super.init(coder: Decoder)
    }

}
