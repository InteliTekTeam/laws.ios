//
//  DocumentListView.swift
//  Laws
//
//  Created by User on 1/24/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import SwipeCellKit

struct DocumentTableParameters{
    let emptyText: String
    let totalText: String
    let totalAmount: Int
    let swipeAble: Bool
    let sortDesc: Bool
    let filter: Bool
    let isSortAble: Bool
}

protocol DocumentListViewDelegate{
    func getDocuments(page: Int, completionHandler: @escaping([Document]?, String?) -> Void)
    func getTableParameters() -> DocumentTableParameters
    func dropFilter() -> Void
    func sort() -> Void
    func didSelect(document: Document, edition: String) -> Void
}

protocol SwipeActionDelegate{
    func didFavorite(document: Document)
    func didDelete(document: Document, index: Int)
}

class DocumentListView: UIView {
    struct Info {
        private(set) var image: UIImage
        private(set) var text: String
        private(set) var color: UIColor
        init(image: UIImage, text: String, color: UIColor) {
            self.image = image
            self.text = text
            self.color = color
        }
    }
    var navigationController: UINavigationController!
    lazy var table: UITableView = {
        var table = UITableView(frame: self.frame)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.estimatedRowHeight = 60.0
        table.rowHeight = UITableViewAutomaticDimension
        table.setEditing(false, animated: true)
        let nib = UINib(nibName: "DocumentTitleTableCell", bundle: nil)
        table.register(nib, forCellReuseIdentifier: "documentTitleCell")
        table.register(InfoCell.self, forCellReuseIdentifier: "infoCell")
        table.register(ButtonCell.self, forCellReuseIdentifier: "buttonCell")
        return table
    }()
    lazy var spinnerView: SpinnerView = {
        var spinnerView = SpinnerView(forAutoLayout: ())
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        return spinnerView
    }()
    
    private var isLoading = false
    
    var delegate: DocumentListViewDelegate?
    var swipeDelegate: SwipeActionDelegate?
    
    private var documents: [Document] = []
    private var page: Int = 1
    private var totalCount = 0
    private var shouldSetupConstraints = true
    var info: Info!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func didMoveToSuperview() {
        self.addSubview(table)
        table.delegate = self
        table.dataSource = self
        table.autoPinEdgesToSuperviewEdges()
        
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: 64.0))
        table.tableFooterView = view
        view.addSubview(spinnerView)
        spinnerView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setDefaults(){
        self.page = 1
        self.documents.removeAll(keepingCapacity: false)
        self.table.reloadData()
        self.requestDocuments()
    }
    
    func deselectCell() -> Bool{
        if let indexPath = self.table.indexPathForSelectedRow{
            self.table.reloadRows(at: [indexPath], with: .automatic)
            return true
        }
        return false
    }
    
    private func startLoading(){
        self.isLoading = true
        info = Info(image: #imageLiteral(resourceName: "info"), text: "Идет загрузка данных. Пожалуйста, подождите.".localized(), color: UIColor.blue)
        self.table.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        self.spinnerView.animate()
    }
    
    private func stopLoading(_ textError: String?){
        self.isLoading = false
        self.spinnerView.stop()
        if self.documents.count==0{
            let text = textError ?? (self.delegate?.getTableParameters().emptyText ?? "Нет данных!".localized())
            self.info = Info(image: #imageLiteral(resourceName: "empty"), text: text, color: UIColor.red)
        }else{
            let totalText = textError ?? (delegate?.getTableParameters().totalText ?? "Нет данных!".localized())
            let total = delegate?.getTableParameters().totalAmount ?? 0
            let text = totalText
            self.totalCount = total
            self.info = Info(image: #imageLiteral(resourceName: "info"), text: text, color: UIColor.green)
        }
        self.table.reloadData()
        if self.page == 1 && self.documents.count > 0 {
            self.table.scrollToRow(at: IndexPath(row: 0, section: 1), at: UITableViewScrollPosition.top, animated: true)
        }
    }
    
    private func requestDocuments(){
        self.startLoading()
        delegate?.getDocuments(page: page, completionHandler: { (documents, errorText) in
            DispatchQueue.main.async{
                if let _ = errorText {
                    self.stopLoading(errorText)
                } else {
                    if let documents = documents {
                        for document in documents {
                            self.documents.append(document)
                        }
                    }
                    self.stopLoading(nil)
                }
            }
        })
    }
}

extension DocumentListView: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if delegate != nil && delegate!.getTableParameters().isSortAble{
                return delegate!.getTableParameters().filter ? 3 : 2
            }
            return 1
        }
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        if section == 0{
            switch indexPath.row{
            case 0:
                let cell = table.dequeueReusableCell(withIdentifier: "infoCell") as! InfoCell
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.setProperties(image: info.image, text: info.text, color: info.color)
                return cell
            case 1:
                let sortText = (delegate?.getTableParameters().sortDesc ?? true)
                    ? "Сортировать по дате ↑".localized()
                    : "Сортировать по дате ↓".localized()
                let cell = table.dequeueReusableCell(withIdentifier: "buttonCell") as! ButtonCell
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.setProperties(text: sortText, type: .sort)
                cell.delegate = self
                return cell
            case 2:
                let cell = table.dequeueReusableCell(withIdentifier: "buttonCell") as! ButtonCell
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.setProperties(text: "Очистить фильтр".localized(), type: .dropFilter)
                cell.delegate = self
                return cell
            default:
                break;
            }
        }else{
            let document = documents[indexPath.row]
            let cell = table.dequeueReusableCell(withIdentifier: "documentTitleCell") as! DocumentTitleTableCell
            cell.setProperties(document: document)
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            return
        }
        if isLoading {
            return
        }
        if totalCount>0 {
            if indexPath.row==documents.count-1 && indexPath.row<totalCount-1{
                self.page+=1
                self.startLoading()
                self.requestDocuments()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            return
        }
        let item = self.documents[indexPath.row]
        delegate?.didSelect(document: item, edition: "last")
    }
    
}

extension DocumentListView: SwipeTableViewCellDelegate{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        guard delegate != nil && delegate!.getTableParameters().swipeAble else { return nil }
        
        let favoriteAction = self.getFavoriteAction()
        let deleteAction = self.getDeleteAction()
        
        return [deleteAction, favoriteAction]
    }
    
    private func getFavoriteAction() -> SwipeAction{
        let action = SwipeAction(style: .default, title: "Избранное".localized()) { action, indexPath in
            self.swipeDelegate?.didFavorite(document: self.documents[indexPath.row])
            self.table.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        action.backgroundColor = UIColor.orange
        action.image = #imageLiteral(resourceName: "star").getColored(color: UIColor.white)
        return action
    }

    private func getDeleteAction() -> SwipeAction{
        let action = SwipeAction(style: .destructive, title: "Удалить".localized()) { action, indexPath in
            self.swipeDelegate?.didDelete(document: self.documents[indexPath.row], index: indexPath.row)
            self.documents.remove(at: indexPath.row)
            self.table.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
        }
        action.backgroundColor = UIColor.red
        action.image = #imageLiteral(resourceName: "trash").getColored(color: UIColor.white)
        return action
    }
    
}

extension DocumentListView: ButtonCellDelegate{
    func tapped(type: ButtonCellType) {
        switch type {
        case .dropFilter:
            delegate?.dropFilter()
        case .sort:
            delegate?.sort()
        }
    }
    
    
}
