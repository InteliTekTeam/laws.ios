//
//  DateTableCell.swift
//  Laws
//
//  Created by User on 1/16/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import DatePickerDialog

protocol DateTableCellDelegate{
    func setFrom(tag: Int, date: Date)
    func setTo(tag: Int, date: Date)
    func resetDate(tag: Int)
}

class DateTableCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toButton: UIButton!
    var delegate: DateTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setLabelSettings()
        setButtonSettings()
        setCellSettings()
        resetValues()
    }
    
    private func setCellSettings(){
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    
    private func setLabelSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.subheadline)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        label.font = titleFont
        label.textColor = UIColor.gray
    }
    
    private func setButtonSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        self.fromButton.titleLabel?.font = titleFont
        self.toButton.titleLabel?.font = titleFont
    }
    
    private func resetValues(){
        self.label.text = ""
        self.resetButtons()
    }
    
    private func resetButtons(){
        self.fromButton.setTitle("нет".localized(), for: UIControlState.normal)
        self.toButton.setTitle("нет".localized(), for: UIControlState.normal)
    }
    
    func setProperies(labelText: String, dateFrom: Date?, dateTo: Date?){
        self.resetValues()
        self.label.text = labelText
        if let dateFrom = dateFrom {
            self.buttonSetDate(button: fromButton, date: dateFrom)
        }
        if let dateTo = dateTo {
            self.buttonSetDate(button: toButton, date: dateTo)
        }

    }

    
    @IBAction func fromAction(_ sender: Any) {
        datePickerTapped(sender: sender, type: "from")
    }
    
    @IBAction func toAction(_ sender: Any) {
        datePickerTapped(sender: sender, type: "to")
    }
    
    @IBAction func resetAction(_ sender: Any) {
        self.delegate?.resetDate(tag: self.tag)
        self.resetButtons()
    }
    
    private func datePickerTapped(sender: Any, type: String){
        let title = type=="from" ? "Дата от".localized() : "Дата до".localized()
        let button = sender as! UIButton
        DatePickerDialog(buttonColor: self.tintColor).show(title, doneButtonTitle: "Готово".localized(), cancelButtonTitle: "Отмена".localized(), defaultDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                if type=="from"{
                    self.delegate?.setFrom(tag: self.tag, date: dt)
                }else{
                    self.delegate?.setTo(tag: self.tag, date: dt)
                }
                self.buttonSetDate(button: button, date: dt)
            }
        }
    }
    
    private func buttonSetDate(button: UIButton, date: Date){
        button.setTitle(date.toString(), for: UIControlState.normal)
    }
}

