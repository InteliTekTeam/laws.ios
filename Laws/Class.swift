//
//  Class.swift
//  Laws
//
//  Created by User on 11/18/17.
//  Copyright © 2017 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Class: Codable {
    var Id: Int
    var Name: ClassName
    var NameLocal: String {
        get{
            if UnitOfWork.shared().settingsService.getCurrentLang() == Lang.ru{
                return Name.Rus
            }else{
                return Name.Kyr ?? Name.Rus
            }
        }
    }
}

struct ClassName: Codable{
    var Rus: String
    var Kyr: String?
}

class ClassRepository: Repository<Class>{
    init() {
        super.init(endpoint: "\(Host)/OpenData/GetClasses.json?&Type=Type")
    }
    
    override func getAll(completionHandler: @escaping ([Class]?, Error?) -> Void) {
        super.getAll { (classes, error) in
            if let error = error {
                completionHandler(nil, error)
            }
            var result = [Class(Id: 0, Name: ClassName(Rus: "Все разделы", Kyr: "Бөлүмдөр"))]
            for clas in classes! {
                if clas.Id == ExcludeClassId{
                    continue
                }
                result.append(clas)
            }
            completionHandler(result, nil)
        }
    }
}
