//
//  GeneralClassifier.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct GeneralClassifier: Codable{
    var Code: String?
    var Name: String
    var GeneralClassifiers: [GeneralClassifier]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.GeneralClassifiers != nil{
            for classificator in self.GeneralClassifiers!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class GeneralClassifierRepository: Repository<GeneralClassifier>{
    init() {
        super.init(endpoint: "\(Host)/OpenData/GetClassificators.json?&Type=GeneralClassifiers")
    }
}
